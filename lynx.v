module top (
   input wire CLK100MHZ,
   input wire LYNX_CKA,
   input wire LYNX_CKB,
   input wire LYNX_CKC,
   input wire [3:0] LYNX_PIXEL,
   input wire LYNX_V,
   input wire LYNX_H,

   output wire [3:0] VGA_R,
   output wire [3:0] VGA_G,
   output wire [3:0] VGA_B,
   //output wire CSYNC,
   output reg VGA_HS,
   output reg VGA_VS

   );

   reg      RGB_EN;
   wire     VGA_CLK;
   reg      [9:0]    VGA_VCOUNT = 0;
   reg      [10:0]   VGA_HCOUNT = 0;

   wire     [11:0]   RAMDATA;

   reg      [14:0]   RD_A;
   reg      [14:0]   RELOAD_RD_A;
   reg      [14:0]   WR_A;
   reg      [14:0]   RELOAD_WR_A = 0;

   reg      [3:0]    RED;
   reg      [3:0]    GREEN;
   reg      [3:0]    BLUE;

   reg      STEPA_DET;
   reg      WE;

   // I believe this simple pll generates a VGA clock
   // and 180-degree out-of-phase clock as well
   // most things happen w/ posedge VGA_CLK
   // one uses "posedge CLOCK_25L".. why not negedge VGA_CLK?
   vgapll   pll1(
     .clk_out1(VGA_CLK),
     .clk_out2(CLOCK_25L),
     // Status and control signals
     .reset(0),
     .locked(),
      // Clock in ports
     .clk_in1(CLK100MHZ)
    );

   // I believe WE is an adhoc clock for writing in pixels to bram
   // not sure what "1" is for (probably write enable)
   // the rest is self-explanatory
   blk_mem_gen_0  ram(
    .clka(WE),
    .ena(1),
    .wea(1),
    .addra(WR_A),
    .dina({RED,GREEN,BLUE}),
    .douta(),

    .clkb(VGA_CLK),
    .enb(1),
    .web(0),
    .addrb(RD_A),
    .dinb(0),
    .doutb(RAMDATA)
    );

   reg      [2:0] STATE;

   assign   CLKMUX = LYNX_CKA & LYNX_CKB & LYNX_CKC;

   assign   STEPA_RISE = (~STEPA_DET & CLKMUX);
   assign   STEPA_FALL = (STEPA_DET & ~CLKMUX);
   assign   STEP = STEPA_RISE | STEPA_FALL | LYNX_H;

   // shouldn't this be XOR?
   // assign   CSYNC = ((VGA_VS) & (VGA_HS));

   always @(posedge STEP) begin
      if (LYNX_H == 1) begin
         if (LYNX_V == 0) begin
            WR_A <= 15'b111111111111111;
            RELOAD_WR_A <= 15'd0;
         end else begin
            RELOAD_WR_A <= RELOAD_WR_A + 15'd160;
            WR_A <= RELOAD_WR_A;
            STATE = 0;
         end
      end else begin
         if (STATE == 0) begin
            RED <= LYNX_PIXEL;
            WR_A <= WR_A + 15'd1;
         end else if (STATE == 1) begin
            GREEN <= LYNX_PIXEL;
            WE = 0;
         end else begin
            BLUE <= LYNX_PIXEL;
            WE = 1;
            STATE = 3'd7;
         end
         STATE = STATE + 3'd1;
      end
   end

   always @(posedge CLOCK_25L) begin
      STEPA_DET <= CLKMUX;
   end

   assign   VGA_R = RGB_EN ? RAMDATA[11:8] : 0;
   assign   VGA_G = RGB_EN ? RAMDATA[7:4] : 0;
   assign   VGA_B = RGB_EN ? RAMDATA[3:0] : 0;

   // I get the feeling this section is pretty sensitive
   // The notes imply that little shifts in the address increments end up
   // screwing up the rendering somehow
   // "presque bon" == "almost good", "nope" == "nope", "pire" == "worse"
   // "133/778 10 !!! YES !!!" == "don't touch anything!" :P

   //134,779 11 est presque bon // 132,777 10 presque bon (bleu) // 131 ? a tester ?
   //130,775 01nope (bleu) 10nope(bleu+) 11nope 00nope
   //131,776 00presque 01pire 11presque 10nope
   //132,777 10nope 01nope 00nope 11nope
   //
   //134: 11-1px 10pire 00-2px 01-3px
   //133/778 10 !!! YES !!!
   //
   //10 ok          01!   00!ok   11/01/00nok
   //
   //138 783 ok    135 ok 133ok  130ok 132/131nok
   always @(posedge VGA_CLK) begin
      if (VGA_HCOUNT < 11'd97) begin
         VGA_HS <= 0;
         RGB_EN <= 0;
      end else if ((VGA_HCOUNT > 11'd133) && (VGA_HCOUNT < 11'd778)) begin
         VGA_HS <= 1;       //Unaligned

         if ((VGA_VCOUNT >= 10'd75) && (VGA_VCOUNT < 10'd484)) begin
            RGB_EN <= 1;
            if (VGA_HCOUNT[1:0] == 2'b10) begin
               RD_A <= RD_A + 15'd1;
            end
         end else begin
            RGB_EN <= 0;
         end
      end else begin
         VGA_HS <= 1;
         RGB_EN <= 0;
      end

      // handle end of vga horizontal line (reset stuff / set up next line)
      if (VGA_HCOUNT == 11'd799) begin
         if (VGA_VCOUNT < 3) // vga vsync is first 3 lines (0-2)
            VGA_VS <= 0;     // active low signal
         else
            VGA_VS <= 1;     // otherwise, not vsyncing

         // we're scaling 4x, so...
         if ((VGA_VCOUNT[1:0] == 2'b01) &&        // if vga vertical line && 0x03 == 1 (every 4 lines)
             (VGA_VCOUNT >= 10'd75) &&            // ... and we're between line 75 ...
             (VGA_VCOUNT < 10'd484)) begin        // ... and line 484 ...
            RELOAD_RD_A <= RELOAD_RD_A + 15'd160; // then setup read address to be on the next line (lynx line width is 160
         end

         VGA_HCOUNT <= 11'd0;

         // end of vga vertical lines, so reset some stuff
         if (VGA_VCOUNT == 10'd525) begin
            VGA_VCOUNT <= 0;                    // start over at vga line 0
            RELOAD_RD_A <= 15'd0;               // prep reload for horizontal end of line 0
            RD_A <= 0;                          // point at beginning of buffer again
         end else begin
            VGA_VCOUNT <= VGA_VCOUNT + 10'd1;   // not the last line, just add 1
         end
         // reset read address to beginning of current line buffer (line multiplying) or next line in buffer
         RD_A <= RELOAD_RD_A;
      end else begin
         // not at end of line, so just increment
         VGA_HCOUNT <= VGA_HCOUNT + 11'd1;
      end
   end

endmodule
