module NGPCTV
(
   input CLOCK_50,
   output [3:0] VGA_R,
   output [3:0] VGA_G,
   output [3:0] VGA_B,
   output reg VGA_VS,
   output reg VGA_HS,
   output [17:0] SRAM_ADDR,
   inout [15:0] SRAM_DQ,
   output SRAM_CE_N,
   output SRAM_LB_N,
   output SRAM_UB_N,
   output SRAM_OE_N,
   output SRAM_WE_N,
   input [3:0] KEY,
   input [9:0] SW,
   input [35:0] GPIO_0
);

reg VGADIV;
reg [9:0] VCOUNT = 0;
reg [9:0] HCOUNT = 0;
reg CLKDIV = 0;
reg VIDOUT = 0;
reg [11:0] PIXEL_IN_PREV;
reg [11:0] PIXEL_IN;
reg [11:0] PIXEL_OUT;
reg [3:0] NGPC_HSYNC_SR;
reg [3:0] NGPC_VSYNC_SR;
reg [1:0] NGPC_DCLK_SR;
reg [17:0] WRITE_ADDR;
reg [17:0] NEXT_WRITE_ADDR;
reg [17:0] READ_ADDR;
reg [17:0] NEXT_READ_ADDR;
reg WRITE_FLAG;
reg [2:0] CYCLE;  // 0~5
reg [1:0] VSCALE;

DEBUGPLL U0(CLOCK_50, CLOCK_100);

assign SRAM_CE_N = 1'b0;
assign SRAM_LB_N = 1'b0;
assign SRAM_UB_N = 1'b0;

assign NGPC_HSYNC = GPIO_0[2];   // Active high
assign NGPC_VSYNC = GPIO_0[3];   // Active low
assign NGPC_DCLK = GPIO_0[1];

assign VGA_R = VIDOUT ? PIXEL_OUT[11:8] : 4'h0;
assign VGA_G = VIDOUT ? PIXEL_OUT[7:4] : 4'h0;
assign VGA_B = VIDOUT ? PIXEL_OUT[3:0] : 4'h0;

always @(posedge CLOCK_100)
begin
   CLKDIV <= ~CLKDIV;
end

assign SRAM_ADDR = (CYCLE < 3'd3) ? READ_ADDR : WRITE_ADDR;

assign SRAM_WE_N = ~(WRITE_FLAG && (CYCLE == 3'd4));
assign SRAM_OE_N = ~(CYCLE == 3'd1);
assign SRAM_DQ = SRAM_OE_N ? {4'b0000, PIXEL_IN} : 16'bzzzzzzzzzzzzzzzz;

always @(posedge CLKDIV)
begin
   VGADIV <= ~VGADIV;
   
   // Detect NGPC VSYNC falling edge
   if (NGPC_VSYNC_SR == 4'b1100)
   begin
      NEXT_WRITE_ADDR <= 18'h00000;
   end
   
   // Detect NGPC HSYNC rising edge
   if (NGPC_HSYNC_SR == 4'b0011)
   begin
      WRITE_ADDR <= NEXT_WRITE_ADDR;
      NEXT_WRITE_ADDR <= NEXT_WRITE_ADDR + 18'd160;
   end
   
   // Detect NGPC DCLK rising edge
   if ((NGPC_DCLK_SR == 2'b01) && (!WRITE_FLAG))
   begin
      WRITE_FLAG <= 1'b1;
      PIXEL_IN <= {4'b0000, GPIO_0[0], GPIO_0[4], 2'b00, 4'b0000};  // Wire all color bits here !
   end
   
   // SRAM access cycle control
   if (CYCLE == 3'd1)
   begin
      PIXEL_OUT <= SRAM_DQ[11:0];
   end
   else if (CYCLE == 3'd5)
   begin
      if (WRITE_FLAG)
      begin
         WRITE_FLAG <= 1'b0;
         WRITE_ADDR <= WRITE_ADDR + 1'b1;
      end
   end
   
   if (CYCLE == 3'd5)
      CYCLE <= 3'd0;
   else
      CYCLE <= CYCLE + 1'b1;
   
   NGPC_DCLK_SR = {NGPC_DCLK_SR[0], NGPC_DCLK};
   NGPC_HSYNC_SR = {NGPC_HSYNC_SR[2:0], NGPC_HSYNC};
   NGPC_VSYNC_SR = {NGPC_VSYNC_SR[2:0], NGPC_VSYNC};
   
   // 25MHz
   if (VGADIV)
   begin
      if ((CYCLE == 3'd3) && VIDOUT)
         READ_ADDR <= READ_ADDR + 1'b1;
      
      // VGA sync gen
      if (HCOUNT < 800)
      begin
         if (HCOUNT < 640)
         begin
            if ((HCOUNT >= 80) && (HCOUNT < 559))
            begin
               if ((VCOUNT >= 12) && (VCOUNT < 467))
                  VIDOUT <= 1;
               else
                  VIDOUT <= 0;
            end
            else
            begin
               VIDOUT <= 0;
            end
            VGA_HS <= 1;
         end
         else if ((HCOUNT >= 640) && (HCOUNT < 664))
         begin
            VIDOUT <= 0;
            VGA_HS <= 1;
         end
         else if ((HCOUNT >= 656) && (HCOUNT < 752))
         begin
            VIDOUT <= 0;
            VGA_HS <= 0;
         end
         else if (HCOUNT >= 760)
         begin
            VIDOUT <= 0;
            VGA_HS <= 1;
         end
         HCOUNT <= HCOUNT + 1;
      end
      else
      begin
         // New raster line
         HCOUNT <= 0;
         
         READ_ADDR <= NEXT_READ_ADDR;
         if ((VCOUNT >= 12) && (VCOUNT < 467))
         begin
            if (VSCALE == 2'd2)
            begin
               VSCALE <= 2'd0;
               NEXT_READ_ADDR <= NEXT_READ_ADDR + 18'd160;
            end
            else
               VSCALE <= VSCALE + 1'b1;
         end
         
         if (VCOUNT < 525)
         begin
            VCOUNT <= VCOUNT + 1;
            if ((VCOUNT >= 490) && (VCOUNT < 493))
            begin
               VGA_VS <= 0;
            end
            else
            begin
               VGA_VS <= 1;
            end
         end
         else
         begin
            //New frame
            VCOUNT <= 0;
            VSCALE <= 0;
            
            NEXT_READ_ADDR <= 18'h00000;
         end
      end
   end
end

endmodule
